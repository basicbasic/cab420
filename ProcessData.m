function [X,Y] =  ProcessData()
% Stub for preproccessing method that reads data in and returns a cell 
% array of the one-hot encoding

% Total number of tweets, I think we have 1600000 or something
numTweets = 1600000;

% Total dimensionality of data, (The number of words we are using to
% predict stuff with, around 366444 or something if i remember correctly)
dimensionality = 1480;
% Put import and preprocessing here
X = zeros(1);
Y = zeros(1);

onehot = load('AFINN96.mat');
%extract table out of struct
onehot = onehot.AFINN96;
%convert to array so that contain() can be used on it
onehot = table2array(onehot);

tweetcontent = load('tweetcontent.mat');
tweetcontent = tweetcontent.tweetcontent;

% X{i} = d * numWords matrix containing one-hot encodings of words and
% their relative order X = a numTweets-long cell array of arrays
X = cell(numTweets,1);
whos
%iterate over every tweet
for tweetNum = 1:numTweets
    %iterate over everyword in the polarity matrix
    for word = 1:dimensionality
        %check if the word is in the tweet
        if contains(onehot(word,:), tweetcontent(tweetNum,:))
            X{tweetNum}(word,:) = 1;
        else
            X{tweetNum}(word,:) = 0;
        end
    end
end

whos
