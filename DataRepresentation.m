% Preface: This data representation format is essential as we can't just
% use normal matrices, as the lengths of each tweet is variable. Messing
% around with cell arrays isn't great but it's the best way to do this in
% matlab imo. Just think of them as normal dynamic lists with 
% indexing using {}

% Example, representation of 3 tweets with 1, 2 and 3 words out of 3
% possible words (d = 3)

% Total number of tweets, I think we have 1600000 or something
numTweets = 3;

% Total dimensionality of data, (The number of words we are using to
% predict stuff with, around 366444 or something if i remember correctly)
dimensionality = 4;

% X{i} = d * numWords matrix containing one-hot encodings of words and 
% their relative order
% X = a numTweets-long cell array of arrays
X = cell(numTweets,1);

% Y(i) = The sentiment label for the ith tweet, we just use a standard
% matrix here as the label size never changes
Y = zeros(numTweets,1);

% Constructing an example Y, you would use a for loop for this
Y = [0;1;1]; %E.g first tweet is negative and rest are positive

% Constructing an example X, again you would use a loop.

TempX1 = [0;0;0;1]; % One word in the tweet that is the fourth word in vocabulary
X{1} = TempX1;

TempX2 = [0 1; 0 0; 1 0; 0 0]; % Two words, the third word followed by the first word
X{2} = TempX2;

TempX3 = [0 0 0; 1 1 1; 0 0 0; 0 0 0]; % Three identical second words
X{3} = TempX3;

% You can now display X{1}, X{2} and X{3} to see the one-hot encoding of
% each tweet
X{1}
X{2}
X{3}

% Test Flatten Data.m
X = FlattenData(X)


